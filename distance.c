#include <stdio.h>
#include <float.h>
#include <math.h>
#include <malloc.h>

void ReadCoordinates(float *x1,float *x2,float *y_1,float *y2);
float CalculateDistance(float *x1,float *x2,float *y_1,float *y2);
void DisplayDistance(float *x1,float *x2,float *y_1,float *y2,float distance);

int main()
{
  float *x1,*x2,*y_1,*y2, distance;
  x1 = (float*)malloc(sizeof(float));
  x2 = (float*)malloc(sizeof(float));
  y_1 = (float*)malloc(sizeof(float));//float *x1,float *x2,float *y_1,float *y2,float distance
  y2 = (float*)malloc(sizeof(float));
  ReadCoordinates(x1,x2,y_1,y2);
  distance = CalculateDistance(x1,x2,y_1,y2);
  DisplayDistance(x1,x2,y_1,y2,distance);
  free (x1);
  free (x2);
  free (y_1);
  free (y2);
  return 0;
}


void ReadCoordinates(float *p1,float *p2,float *p3,float *p4)
{
   float x, y;
   printf("Enter the first coordinates\n");
   scanf("%f%f",&x,&y);
   *p1 = x;
   *p3 = y;
   printf("Enter the second coordinates\n");
   scanf("%f%f",&x,&y);
   *p2 = x;
   *p4 = y;
}

float CalculateDistance(float *x1,float *x2,float *y_1,float *y2)
{
	float distance = sqrt(pow(*x2-*x1,2)+pow(*y2-*y_1,2));
	return distance;
}

void DisplayDistance(float *x1,float *x2,float *y_1,float *y2,float distance)
{
 printf("The distance between the two given coordinates (%.2f,%.2f),(%.2f,%.2f) = %f\n",*x1,*y_1,*x2,*y2,distance);
  return; 
}




