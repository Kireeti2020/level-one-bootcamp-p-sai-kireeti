#include <stdio.h>
#include<stdlib.h>

struct Fraction
{

int Numerator;
int Denominator;

};

void  ReadFractions(struct Fraction *frac1,struct Fraction *frac2);
int CalculateSum(struct Fraction *frac1,struct Fraction *frac2,int l);
void DisplaySum(struct Fraction *frac1,struct Fraction *frac2,int l,int sum);
int checkValidityofDen(int den);
int lcm(struct Fraction *frac1,struct Fraction *frac2);



int main()
{   
    int sum=0;
    struct Fraction *frac1,*frac2;
    frac1 = (struct Fraction*)malloc(sizeof(struct Fraction));
    frac2 = (struct Fraction*)malloc(sizeof(struct Fraction));
    ReadFractions(frac1,frac2);
    int l = lcm(frac1,frac2);
    sum = CalculateSum(frac1,frac2,l);
    DisplaySum(frac1,frac2,l,sum);
    free(frac1);
    free(frac2);

    return 0;
}


void ReadFractions(struct Fraction *frac1,struct Fraction *frac2)
{
  int num1, num2,den1,den2,den;
  printf("Enter the First Fraction Details\n");
  printf("\nEnter the Numerator of the First Fraction :");
  scanf("%d",&num1);
  printf("Enter the Denominator of the First Fraction :");
  scanf("%d",&den1);
  den = checkValidityofDen(den1);
  frac1->Numerator = num1;
  frac1->Denominator = den;
  printf("\nEnter the Second Fraction Details\n");
  printf("\nEnter the Numerator of the Second Fraction :");
  scanf("%d",&num2);
  printf("Enter the Denominator of the second Fraction :");
  scanf("%d",&den2);
  den = checkValidityofDen(den2);
  frac2->Numerator = num2;
  frac2->Denominator = den;

  
 }
  
int checkValidityofDen(int den)
{
      if(den != 0 )
	    return den;
      if( den == 0)
	  { 
	    int Valid = 0;
        while(!Valid)
	    {   
	     
	      printf("\nError: DIVIDING WITH ZERO YIELDS INFINITY ...!!! So can you re-enter valid value\n");
	      printf("Enter 1 to provide a valid value , If you want to abort  press any key other than 1, \n");
		  int ch;
	      scanf("%d",&ch);
	      switch(ch)
	      {
	          case 0: printf("EXITING..!!\n");
	                exit(0);
	          default : printf("\nEnter the valid Denominator  of the Fraction :");
                    scanf("%d",&den);
					if(checkValidityofDen(den))
				     {
					   Valid = 1;
					   break;
					 }
	      }
		  
		}
	  }	
}



int lcm(struct Fraction *frac1,struct Fraction *frac2)
{
    int a=(int)frac1->Denominator;
    int b=(int)frac2->Denominator;
    int r=0;
    while(1)
    {
      if(a%b==0)
      {
          break;
      }  
      r=a%b;
      a=b;
      b=r;
    }
    
    int result = ((frac1->Denominator*frac2->Denominator)/b);
    return(result);
}




int CalculateSum(struct Fraction *frac1,struct Fraction *frac2,int l)
{
  int n1,n2;
  n1 = (int)((l*frac1->Numerator)/frac1->Denominator);
  n2 = (int)((l*frac2->Numerator)/frac2->Denominator);
  return (n1+n2);
}

void DisplaySum(struct Fraction *frac1,struct Fraction *frac2,int l,int sum)
{
 printf("\nThe sum of the given two Fractions : ( %d / %d ) + ( %d / %d ) = ( %d / %d ) ", frac1->Numerator,frac1->Denominator,  frac2->Numerator, frac2->Denominator ,sum,l);
 return;
}
