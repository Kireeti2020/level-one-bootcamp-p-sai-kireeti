#include <stdio.h>
#include <float.h>
#include <math.h>
#include <malloc.h>

struct coordinates
{
 float x;
 float y;
};


void ReadCoordinates(struct coordinates *c1,struct coordinates *c2);
float CalculateDistance(struct coordinates *c1,struct coordinates *c2);
void DisplayDistance(struct coordinates *c1,struct coordinates *c2,float distance);




int main()
{
  float distance; 
  struct coordinates *c1,*c2;
  c1= (struct coordinates*)malloc(sizeof(struct coordinates));
  c2= (struct coordinates*)malloc(sizeof(struct coordinates));
  ReadCoordinates(c1,c2);
  distance=CalculateDistance(c1,c2);
  DisplayDistance(c1,c2,distance);
  free(c1);
  free(c2);
  return 0;
}


void ReadCoordinates(struct coordinates *c1,struct coordinates *c2)
{
   float x1,x2,y1,y2;
   printf("Enter the first coordinates\n");
   scanf("%f%f",&x1,&y1);
   printf("Enter the second coordinates\n");
   scanf("%f%f",&x2,&y2);
   c1->x = x1;
   c1->y = y1;
   c2->x = x2;
   c2->y = y2;
   return;  
}

float CalculateDistance(struct coordinates *c1,struct coordinates *c2)
{
	float distance = sqrt(pow(c2->x-c1->x,2)+pow(c2->y-c1->y,2));
	return distance;
}

void DisplayDistance(struct coordinates *c1,struct coordinates *c2,float distance)
{
 printf("The distance between the two given coordinates ( %.2f, %.2f) , (%.2f , %.2f) = %f\n",c1->x,c1->y,c2->x,c2->y,distance);
  return; 
}
