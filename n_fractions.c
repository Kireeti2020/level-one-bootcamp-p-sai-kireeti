#include<stdio.h>
#include<malloc.h>

struct Fraction 
{
	int Num;
	int Den;
};

int gcd(int a, int b)
{
	if (b==0)
	{
		return a;
}
return gcd(b,a%b);
}

int findlcm(struct Fraction *frac1, int n)
{
	int ans = frac1->Den;
	frac1++;
	for (int i=1;i<n;i++)
	{
	ans = (((frac1->Den * ans )) / (gcd(frac1->Den,ans)));
	frac1++;
	
}
return ans;
}
void addReduce(int n, struct Fraction *frac1)
{

	int final_num = 0;
	int final_den = findlcm(frac1, n);
	for (int i = 0; i < n; i++)
	{
		final_num = final_num+ (frac1->Num) * (final_den/ frac1->Den);
		frac1++;
	}
	int GCD = gcd(final_num,
				final_den);

	final_num /= GCD;
	final_den /= GCD;
	printf("Final Sum frac1 = %d /%d \n", final_num, final_den);
}

int  main()
{	
	int n;
printf("Enter the value of n \n");
	scanf("%d",&n);
	struct Fraction *frac1, *frac2;
    frac1 = (struct Fraction*)malloc(n*sizeof(struct Fraction));
    frac2 = frac1;
	int arr1[n], arr2[n];
    for(int i =0;i<n;i++)
	{
	    printf("Enter the numerator of %d  fraction\n", i+1);
	    scanf("%d",&arr1[i]);
	    printf("Enter the denominator of %d  fraction\n", i+1);
	    scanf("%d",&arr2[i]);
	    frac1->Num = arr1[i];
	    frac1->Den = arr2[i];
	    frac1++;
	}
	addReduce(n, frac2);
	return 0;
}